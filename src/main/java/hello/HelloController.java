package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.net.InetAddress;

@RestController
public class HelloController {
    
    @RequestMapping("/")
    public String index() throws java.net.UnknownHostException {

        String buildNumber = System.getenv("BUILD_NUMBER");
        String deployNumber = System.getenv("DEPLOY_NUMBER");

        String hostname = InetAddress.getLocalHost().getHostName();
        String[] parts = hostname.split("-");
        String color = parts[0];
        return "<html><title>" + color + "</title><body>" +
                "<h3 style='color: " + color + ";'>Host: " +
                "<code>" + hostname + "</code>" +
                "; Deploy number: " + deployNumber + "</h3> HELLO AGAIN" +
                "</body></html>";
    }
    
}
